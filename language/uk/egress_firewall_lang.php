<?php

$lang['egress_firewall_allow_all'] = 'Дозволити все';
$lang['egress_firewall_allow_all_and_specify_block_destinations'] = 'Дозволити весь вихідний трафік - окрім адресатів блокування';
$lang['egress_firewall_app_description'] = 'Додаток Вихідний брандмауер дозволяє вам блокувати певні види трафіку, які залишають вашу мережу.';
$lang['egress_firewall_app_name'] = 'Вихідний брандмауер';
$lang['egress_firewall_block_all'] = 'Заблокувати все';
$lang['egress_firewall_destination_domains'] = 'Домени призначення';
$lang['egress_firewall_destination_ports'] = 'Порт(и) призначення';
$lang['egress_firewall_mode'] = 'Режим';
$lang['egress_firewall_web_lang_block_all_and_specify_allow_destinations'] = 'Блокувати весь вихідний трафік - окрім дозволених адресів';
